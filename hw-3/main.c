#include "cdfh.h"
#include "jpeg.h"
#include "zip.h"

#include <stdlib.h>

void print_usage(const char *prog_name) {
    printf("Wrong command line arguments!\nUsage: %s <rarjpeg file name>\n", prog_name);
}

int main(int argc, const char *argv[]) {
    if (argc == 1) {
        print_usage(argv[0]);
        return 0;
    }

    const char *filename = argv[1];
    printf("Analyzing file \"%s\"\n", argv[1]);

    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        fprintf(stderr, "Failed to open file %s\n", filename);
        exit(EXIT_FAILURE);
    }

    int err = is_pure_jpeg(fp);

    if (err == 0) {
        printf("%s is a pure jpeg file\n", filename);
        exit(EXIT_SUCCESS);
    }

    if (err == 1) {
        fprintf(stderr, "%s is not a jpeg file\n", filename);
        exit(EXIT_FAILURE);
    }

    advance_to_jpeg_finish(fp);

    err = list_files_from_zip_archive(fp);
    if (err == 1) {
        fprintf(stderr, "Error while reading zip format content\n");
        exit(EXIT_FAILURE);
    }

    fclose(fp);
    return 0;
}
