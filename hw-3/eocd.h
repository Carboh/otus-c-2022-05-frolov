#ifndef HW_3_EOCD_H
#define HW_3_EOCD_H

#define kEndOfCentralDirectorySignature 0x06054b50
#define kEndOfCentralDirectoryBaseSize 22

#pragma pack(push, 1)
struct EndOfCentralDirectory {
    // Обязательная сигнатура, равна 0x06054b50
    uint32_t signature;

    // Номер диска
    uint16_t diskNumber;

    // Номер диска, где находится начало Central Directory
    uint16_t startDiskNumber;

    // Количество записей в Central Directory в текущем диске
    uint16_t numberCentralDirectoryRecord;

    // Всего записей в Central Directory
    uint16_t totalCentralDirectoryRecord;

    // Размер Central Directory
    uint32_t sizeOfCentralDirectory;

    // Смещение Central Directory
    uint32_t centralDirectoryOffset;

    // Длина комментария
    uint16_t commentLength;

    // Комментарий (длиной commentLength)
    uint8_t *comment;
};
#pragma pack(pop)

#endif //HW_3_EOCD_H
