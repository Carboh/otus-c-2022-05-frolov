#include "jpeg.h"
#include <stdint.h>

int is_pure_jpeg(FILE *fp) {
    long start_pos = ftell(fp);

    uint16_t starts_with;
    fread(&starts_with, sizeof(uint16_t), 1, fp);
    const uint16_t kJPEGFileStartSignature = 0xD8FF;
    if (starts_with != kJPEGFileStartSignature) {
        // return to start position
        fseek(fp, start_pos, SEEK_SET);
        return 1;
    }

    // advance to end of file
    fseek(fp, -2, SEEK_END);

    const uint16_t kJPEGFileFinishSignature = 0xD9FF;
    uint16_t finishes_with;
    fread(&finishes_with, sizeof(uint16_t), 1, fp);
    if (finishes_with != kJPEGFileFinishSignature) {
        // return to start position
        fseek(fp, start_pos, SEEK_SET);
        return 2;
    }

    return 0;
}

void advance_to_jpeg_finish(FILE *fp) {
    int ch = getc(fp);
    while (ch != EOF) {
        int prev_char = ch;
        ch = getc(fp);
        if (prev_char == 0xFF && ch == 0xD9) {
            return;
        }
    }
}

