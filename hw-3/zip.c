#include "cdfh.h"
#include "eocd.h"
#include "zip.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define kTerminatingNullCharacter '\0'

void *xmalloc(size_t size) {
    void *value = malloc(size);
    if (value == NULL) {
        perror("virtual memory exhausted");
    }
    return value;
}

int list_files_from_zip_archive(FILE *fp) {
    long zip_start_pos = ftell(fp);

    if (fseek(fp, -kEndOfCentralDirectoryBaseSize, SEEK_END) != 0) {
        perror("Error while reading zip format\n");
        return -1;
    }

    struct EndOfCentralDirectory eocd;
    size_t cnt = fread(&eocd, sizeof(struct EndOfCentralDirectory), 1, fp);
    if (cnt != 0) {
        perror("Error while reading zip format\n");
        return -1;
    }
    assert(eocd.signature == kEndOfCentralDirectorySignature);

    fseek(fp, zip_start_pos, SEEK_SET);

    if (fseek(fp, eocd.centralDirectoryOffset, SEEK_CUR) != 0) {
        perror("Error while reading zip format\n");
        return -1;
    }

    for (int i = 0; i < eocd.totalCentralDirectoryRecord; ++i) {
        struct CentralDirectoryFileHeader cdfh;
        fread(&cdfh, kCentralDirectoryFileHeaderBaseSize, 1, fp);
        assert(cdfh.signature == kCentralDirectoryFileHeaderSignature);

        size_t strlen = (size_t) cdfh.filenameLength + cdfh.extraFieldLength + cdfh.fileCommentLength;
        char *str = xmalloc(strlen + 1);
        cnt = fread(str, strlen, 1, fp);
        if (cnt != 1) {
            perror("Error while reading zip format\n");
        }
        str[cdfh.filenameLength] = kTerminatingNullCharacter;
        printf("%s\n", str);
        free(str);
    }

    return 0;
}
