#ifndef HW_3_JPEG_H
#define HW_3_JPEG_H

#include <stdio.h>

int is_pure_jpeg(FILE *fp);
void advance_to_jpeg_finish(FILE *fp);

#endif //HW_3_JPEG_H
