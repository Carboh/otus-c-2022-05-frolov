#include <stdio.h>
#include <stdlib.h>

#include "cp1251.h"
#include "encoding.h"

void print_usage(const char *prog_name) {
    printf("Wrong command line arguments!\nUsage: %s input.file encoding_name output.file\n", prog_name);
    printf("Note: only CP-1251 encoding is supported for now");
}

int main(int argc, const char *argv[]) {
    if (argc != 4) {
        print_usage(argv[0]);
        return 0;
    }

    const char *input_filename = argv[1];
    printf("Input file is \"%s\"\n", input_filename);

    const char *encoding = argv[2];
    printf("Encoding is \"%s\"\n", encoding);

    const char *output_filename = argv[3];
    printf("Output file is \"%s\"\n", output_filename);

    FILE *in = fopen(input_filename, "r");
    if (in == NULL) {
        fprintf(stderr, "Failed to open file %s\n", input_filename);
        exit(EXIT_FAILURE);
    }


    FILE *out = fopen(output_filename, "wb");
    if (out == NULL) {
        fprintf(stderr, "Failed to open file %s for writing\n", output_filename);
        exit(EXIT_FAILURE);
    }


    encoding_type encoding_t = cp1251;
    switch (encoding_t) {
        case cp1251:
            decode_cp1251_to_utf(in, out);
            break;
        case koi8_r:
            break;
        case iso_8859_5:
            break;
        default:
            exit(EXIT_FAILURE);
    }

    return 0;
}
