#ifndef HW_4_COMMON_H
#define HW_4_COMMON_H

#include <stdlib.h>
#include <stdint.h>

#define kTwoByteSequenceLength 2

typedef struct {
    uint8_t first_char;
    uint8_t second_char;
} utf8_2byte_sequence;

#endif //HW_4_COMMON_H
