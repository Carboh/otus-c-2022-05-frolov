#ifndef HW_4_CP1251_H
#define HW_4_CP1251_H

#include <stdio.h>

void decode_cp1251_to_utf(FILE *in, FILE *out);

#endif //HW_4_CP1251_H
