#ifndef HW_4_ENCODING_H
#define HW_4_ENCODING_H

typedef enum {
    cp1251,
    koi8_r,
    iso_8859_5,
} encoding_type;

#endif //HW_4_ENCODING_H
